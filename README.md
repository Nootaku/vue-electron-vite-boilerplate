# Electron Application with Vue.js and Vite

![](/home/nootaku/Pictures/VOV.png)



> In this exercise we will create a boilerplate for Electron applications with a Vue.js frontend framework and Vite Tooling.
>
> Note:<br/>At the time of writing the versions are: ***Vite v3, Vue v3.2.41, Electron 21.0.1***

## 1. Project setup

There are multiple ways to do this. The first is, of course, to use the [Awsome-Vite](https://github.com/vitejs/awesome-vite) and it's built-in tool. It is simple and efficient for prototyping.

```bash
npm create electron-vite@latest
cd electron-vite-vue
npm install
```

This method is MIT licensed and will not be suitable for deployment (esp. for packaging).

### Building and understanding

Another way of doing things is to build it yourself. Let's start with the basics.

#### Create a Vue project using Vite

> [documentation](https://vitejs.dev/guide/)

```bash
npm create vite@latest
>>> Project name: foo
>>> Select a framework: Vue
>>> Select a variant: JavaScript

cd foo
npm install

# Check that everything works
npm run dev
```

#### Installing Electron

```bash
npm install --save-dev electron
```

A basic Electron app needs 4 files to run:

- `package.json`
- `main.js`
- `preload.js`
- `index.html`

With our current setup, we already have the `package.json`. While our directory does contain a file called `main.js` and `index.html` files, those are actually the onces we need for Vite, not the ones we need for Electron. The Vite files are used to run our Vite app, but in Electron, the `main.js` will create the browser window and load the `index.html` file that will **contain our Vite app**.

So let's start by creating our Vite app.

## 2. Build Vite App for Electron

### Asset references

To be configure our Vite app for Electron we will have to change the **asset references**. This means to make sure that, when our project is built, all the references to the javascript and css files are pointing to the accurate spot.

When we build a Vite application creates a `dist` folder with the following structure:<br/><img src="/home/nootaku/Pictures/Screenshots/vite_build.png" style="zoom:30%;" />

There is an `index.html` file and an `assests` folder. But since our Electron code is in the root directory we want to set the base_path of our project to be this `/dist` folder.

This can be done by editing `vite.config.js` in our root directory:

```js
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
  base: path.resolve(__dirname, './dist')
  plugins: [vue()]
})
```

Now we can go back to our terminal and build our `/dist` directory:

```bash
npm run build
```

This should work, but there is also the possibility to have an error saying `Error: Dynamic require of "path" is not supported`. In that case, I managed to make it work by removing `type: "module"` from `package.json` (as found [here](https://github.com/Subwaytime/vite-aliases/issues/33#issuecomment-1064409374)).

### Electron `main.js`

Now that our references have been sorted out, we can create our `main.js` (for Electron) at the root of our project. And paste the content of the [Electron Quick Start guide](https://www.electronjs.org/docs/latest/tutorial/quick-start#recap) in the created file.

> We can also use the version of the [Tutorial - first app](https://www.electronjs.org/docs/latest/tutorial/tutorial-first-app#final-starter-code)

However we need to make one change: The path to the `index.html`.

```js
// ...

  // and load the index.html of the app.
  mainWindow.loadFile('dist/index.html')

// ...
```

### Electron `preload.js`

Again, make create a `preload.js` at the root of our project and paste the code from the [Electron Quick Start guide](https://www.electronjs.org/docs/latest/tutorial/quick-start#recap). No changes required this time.

## 3. Final changes

The final changes take place in the `package.json` file. By default Electron looks for an `index.js` file to start our app, but since our entrypoint is called `main.js` we need to specify this in `package.json`.

Moreover, we haven't got any way to run our electron app. The [electron documentation](https://www.electronjs.org/docs/latest/tutorial/quick-start#scaffold-the-project) specifies that we need to add a `start` command to the `scripts` in `package.json`.

Let's do both:

```json
{
  "name": "test_electron",
  "private": true,
  "version": "0.0.0",
  "main": "main.js",
  "scripts": {
    "dev": "vite",
    "build": "vite build",
    "preview": "vite preview",
    "electron:start": "electron ."
  },
  "dependencies": {
    "vue": "^3.2.41"
  },
  "devDependencies": {
    "@vitejs/plugin-vue": "^3.2.0",
    "electron": "^21.2.0",
    "vite": "^3.2.0"
  }
}
```

We can now run the Electron app with the command:

```bash
npm run electron:start
```

## 4. Editing the template

This is the crappy bit. Electron only has access to the built Vite app. In other words, no _hot reaload_. However, the frontend of the application does. This means that we will build our frontend by running

```bash
npm run dev
```

Then for Electron we will run:

```bash
npm run build
npm run electron:start
```
